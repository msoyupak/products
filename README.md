E-Commerce API
========

Learning to build a RESTful API using node.js, express and MongoDB.

Following post: http://pixelhandler.com/blog/2012/02/09/develop-a-restful-api-using-node-js-with-express-and-mongoose/

Sample update request: 

curl -H "Content-Type: application/json"  -d '{"title" : "Great Flip-Flop", "description" : "Some flip flops are meant to be shown", "style" : "Black small", "images": [{"kind":"thumbnail","url":"images/products/1234/main.jpg"}], "categories":[{"name":"Clothes"},{"name":"Shirts"}], "catalogs":[{"name":"Summer"}], "variants":[{"color":"White","images":[{"kind":"thumbnail","url":"images/products/1234/variant1.jpg"}],"sizes":[{"size":"M","available":"123","sku":"23132asd","price":"54"}]}]}' -X PUT "http://localhost:4242/api/products/509f04066d37900000000001"

Sample response:

[
  {
    "__v": 6,
    "_id": "509f04066d37900000000001",
    "description": "Some flip flops are meant to be shown",
    "style": "Black small",
    "title": "Great Flip-Flop",
    "modified": "2012-11-11T01:48:54.960Z",
    "variants": [
      {
        "color": "White",
        "_id": "509fe9961d655d0000000012",
        "sizes": [
          {
            "size": "M",
            "available": 123,
            "sku": "23132asd",
            "price": 54,
            "_id": "509fe9961d655d0000000013"
          }
        ],
        "images": [
          {
            "kind": "thumbnail",
            "url": "images/products/1234/variant1.jpg",
            "_id": "509fe9961d655d0000000014"
          }
        ]
      }
    ],
    "catalogs": [
      {
        "name": "Summer",
        "_id": "509fe9961d655d0000000011"
      }
    ],
    "categories": [
      {
        "name": "Clothes",
        "_id": "509fe9961d655d0000000010"
      },
      {
        "name": "Shirts",
        "_id": "509fe9961d655d000000000f"
      }
    ],
    "images": [
      {
        "kind": "thumbnail",
        "url": "images/products/1234/main.jpg",
        "_id": "509fe9961d655d000000000e"
      }
    ]
  }
]