var application_root = __dirname,
	express = require('express'),
	path = require('path'),
	mongoose = require('mongoose');

var app = express();

// Database

mongoose.connect('mongodb://localhost/ecomm_database');

// Config

app.configure(function () {
	app.use(express.bodyParser());
	app.use(express.methodOverride());
	app.use(app.router);
	app.use(express.static(path.join(application_root, "public")));
	app.use(express.errorHandler({dumpExceptions: true, showStack: true}));
});

var Schema = mongoose.Schema;

var Sizes = new Schema({
	size: {type: String, required: true },
	available: {type: Number, required: true, min: 0, max: 1000 },
	sku: {
		type: String,
		required: true,
		validate: [/[a-zA-Z0-9]/, 'Product sku should only have letters and numbers']
	},
	price: {type: Number, required: true, min: 0}
});

var Images = new Schema({
	kind: {
		type: String,
		enum: ['thumbnail', 'catalog', 'detail', 'zoom'],
		required: true
	},
	url: {type: String, required: true}
});

var Variants = new Schema({
	color: String,
	images: [Images],
	sizes: [Sizes]
});

var Categories = new Schema({
	name: String
});

var Catalogs = new Schema({
	name: String
});

var Product = new Schema({
	title: { type: String, required: true },
	description: { type: String, required: true },
	style: { type: String, unique: true },
	images: [Images],
	categories: [Categories],
	catalogs: [Catalogs],
	variants: [Variants],
	modified: { type: Date, default: Date.now }
});

var ProductModel = mongoose.model('Product', Product);

app.get('/api', function (req, res) {
	res.send('Ecomm API is running');
});

app.get('/api/products', function(req, res){
	return ProductModel.find(function (err, products) {
		if(!err) {
			return res.send(products);
		} else{
			return console.log(err);
		}
	});
});

app.post('/api/products', function (req, res){
	var product;
	console.log("POST: ");
	console.log(req.body);
	product = new ProductModel({
		title: req.body.title,
		description: req.body.description,
		style: req.body.style,
		images: req.body.images,
		categories: req.body.categories,
		catalogs: req.body.catalogs,
		variants: req.body.variants
	});
	product.save(function(err){
		if(!err){
			return console.log("created");
		} else {
			return console.log(err);
		}
	});
	return res.send(product);
});

app.get('/api/products/:id', function (req, res){
	return ProductModel.findById(req.params.id, function(err, product){
		if(!err){
			return res.send(product);
		} else {
			return console.log(err);
		}
	});
});

app.put('/api/products/:id', function (req, res){
	return ProductModel.findById(req.params.id, function(err, product){
		product.title = req.body.title;
		product.description = req.body.description;
		product.style = req.body.style;
		product.images = req.body.images;
		product.categories = req.body.categories;
		product.catalogs = req.body.catalogs;
		product.variants = req.body.variants;
		return product.save(function(err) {
			if(!err){
				console.log('updated');
			} else {
				console.log(err);
			}

			return res.send(product);
		});
	});
});

app.delete('/api/products/:id', function (req, res){
	return ProductModel.findById(req.params.id, function(err, product) {
		return product.remove(function(err){
			if(!err){
				console.log('deleted');
				return res.send('');
			}
			else{
				return console.log(err);
			}
		})
	});
});

app.listen(4242);